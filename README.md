# Set of Mac OS X keyboard layouts (input sources)
 * **Netsu Dvorak** — a bit changed version of common *Programmer Dvorak* without shifting the digital row;
 * **Netsu Russian** — *Russian (Typewriter)* and/or *Russian (Apple)* layout redesigned to be compatible with *Netsu Dvorak* as much as possible (keyboard shortcuts compatible too);
 * **Netsu Russian - Phonetic** — russian phonetic keyboard layout designed to be compatible with *Netsu Dvorak*;
 * **Netsu Ukrainian** — ukrainian layout based on *Netsu Russian*;
 * **Netsu Ukrainian - Phonetic** — ukrainian layout based on *Netsu Russian - Phonetic*.
## Installation
#### for single user:
    cd ./Keylayouts/ && cp ./*.bundle ~/Library/Keyboard\ Layouts/ && tell application "System Events" to log out

#### system-wide:
    cd ./Keylayouts/ && sudo cp ./*.bundle /Library/Keyboard\ Layouts/ && sudo shutdown -r now

## Screenshots
![Netsu Dvorak](https://bitbucket.org/Kamiel/netsu-programmer-dvorak/raw/1fe181d4820c/Screenshots/Netsu%20Dvorak.png)
![Netsu Dvorak shifted](https://bitbucket.org/Kamiel/netsu-programmer-dvorak/raw/1fe181d4820c/Screenshots/Netsu%20Dvorak%20shifted.png)
![Netsu Ukrainian](https://bitbucket.org/Kamiel/netsu-programmer-dvorak/raw/1fe181d4820c/Screenshots/Netsu%20Ukrainian.png)
***
## References
- [Common Programmer Dvorak](http://www.kaufmann.no/roland/dvorak/)
- [Russian Linux layout for Programmer Dvorak](https://github.com/binarin/russian-programmer-dvorak)
- [Custom Typewriter layout for Mac](http://www.dotkam.com/2011/01/04/edit-keyboard-layout-on-mac/)
- [Ukelete utilitie](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=ukelele)
- [KeyRemap4MacBook utilitie](http://pqrs.org/macosx/keyremap4macbook/index.html.en)
- [PCKeyboardHack utilitie](http://pqrs.org/macosx/keyremap4macbook/pckeyboardhack.html.en)
- [NoEjectDelay utilitie](http://pqrs.org/macosx/keyremap4macbook/noejectdelay.html.en)
